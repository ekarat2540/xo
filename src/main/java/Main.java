
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ekara
 */
public class Main {

    static char table[][] = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static int row, col;
    static char player = 'X';
    static int turn = 0;

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            input();
            if (checkWin()) {
                break;
            }
            switchPlayer();
        }
        showTable();
        showWin();
        showBye();
    }

    private static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private static void showTurn() {
        System.out.println(player + " turn");
    }

    private static void input() {
        while (true) {
            try {
                Scanner sc = new Scanner(System.in);
                System.out.println("Please input Row Col: ");
                String input = sc.nextLine();
                String str[] = input.split(" ");
                if (str.length != 2) {
                    System.out.println("Pls input row Col");
                    continue;
                }
                row = Integer.parseInt(str[0]);
                col = Integer.parseInt(str[1]);
                if(row > 3 || row < 1 || col > 3 || col < 1){
                    System.out.println("Pls input row Col");
                    continue;
                }
                if (!setTable()) {
                    System.out.println("Pls input row Col");
                    continue;
                }
                break;
            } catch (Exception e) {
                System.out.println("Pls input row Col");
                continue;
            }

        }
    }

    private static boolean checkWin() {
        if (checkRow()) {
            return true;
        }
        if (checkCol()) {
            return true;
        }
        if (checkX()) {
            return true;
        }
        if (isDraw()) {
            return true;
        }
        return false;

    }

    private static boolean isDraw() {
        if (turn == 8) {
            return true;
        }
        return false;
    }

    private static void showWin() {
        if (isDraw()) {
            System.out.println("Draw");
        } else {
            System.out.println(player + "Win....");

        }
    }

    private static void showBye() {
        System.out.println("Bye Bye");
    }

    private static void showTable() {
        System.out.println(" 1 2 3 ");
        for (int rowInd = 0; rowInd < table.length; rowInd++) {
            System.out.print(rowInd + 1);
            for (int colInd = 0; colInd < table.length; colInd++) {
                // - - -
                System.out.print(" " + table[rowInd][colInd]);
            }
            System.out.println();
        }
    }

    private static void switchPlayer() {

        player = player == 'X' ? 'O' : 'X';
        turn++;
    }

    private static boolean setTable() {
        if (table[row - 1][col - 1] != '-') {

            return false;
        }
        table[row - 1][col - 1] = player;
        return true;
    }

    private static boolean checkRow(int rowInd) {
        for (int colInd = 0; colInd < table[rowInd].length; colInd++) {
            if (table[rowInd][colInd] != player) {
                return false;
            }
        }

        return true;
    }

    private static boolean checkRow() {
        for (int rowInd = 0; rowInd < table.length; rowInd++) {
            if (checkRow(rowInd)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(int colInd) {
        for (int rowInd = 0; rowInd < table[colInd].length; rowInd++) {
            if (table[rowInd][colInd] != player) {
                return false;
            }
        }

        return true;
    }

    private static boolean checkCol() {
        for (int colInd = 0; colInd < table.length; colInd++) {
            if (checkCol(colInd)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkX() {
        if (checkX1()) {
            return true;
        }
        if (checkX2()) {
            return true;
        }
        return false;
    }

    private static boolean checkX1() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != player) {
                return false;
            }
        }
        return true;
    }

}
